package br.com.calculadora.models;

import java.util.ArrayList;
import java.util.List;

public class Calculadora {
    private List<Integer> numeros = new ArrayList<>();

    public Calculadora() {
    }

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}
