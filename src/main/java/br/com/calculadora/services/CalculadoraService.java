package br.com.calculadora.services;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

//quando tdods chamar vai receber como serviço a calculadora_services
@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora) {
        double resultado = 0;
        for (Integer numero : calculadora.getNumeros()) {
            resultado = resultado + numero;
        }

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO subtrair(Calculadora calculadora){
        double resultado = 0;

        //resultado = calculadora.getNumeros().get(0) - calculadora.getNumeros().get(1);

        if(calculadora.getNumeros().get(0) < calculadora.getNumeros().get(1))
        {
            resultado = Double.parseDouble(calculadora.getNumeros().get(1).toString()) - Double.parseDouble(calculadora.getNumeros().get(0).toString());
        }
        else{
            resultado = Double.parseDouble(calculadora.getNumeros().get(0).toString()) - Double.parseDouble(calculadora.getNumeros().get(1).toString());
        }

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO dividir(Calculadora calculadora){
        double resultado = 0;

        if(calculadora.getNumeros().get(0) < calculadora.getNumeros().get(1))
        {
            resultado = Double.parseDouble(calculadora.getNumeros().get(1).toString()) / Double.parseDouble(calculadora.getNumeros().get(0).toString());
        }
        else{
            resultado = Double.parseDouble(calculadora.getNumeros().get(0).toString()) / Double.parseDouble(calculadora.getNumeros().get(1).toString());
        }

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO multiplicar(Calculadora calculadora){
        double resultado = 0;

        resultado = Double.parseDouble(calculadora.getNumeros().get(0).toString()) * Double.parseDouble(calculadora.getNumeros().get(1).toString());

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }
}
