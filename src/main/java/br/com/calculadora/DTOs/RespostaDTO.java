package br.com.calculadora.DTOs;

public class RespostaDTO {
    private Double resultado;

    public RespostaDTO() {
    }

    public RespostaDTO(Double resultado) {
        this.resultado = resultado;
    }

    public Double getResultado() {
        return resultado;
    }

    public void setResultado(Double resultado) {
        this.resultado = resultado;
    }
}
