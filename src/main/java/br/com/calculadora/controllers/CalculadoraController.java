package br.com.calculadora.controllers;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.server.ResponseStatusException;

@RestController //faz a classe se transformar em uma controladora @REstcontroller (programação orientada a aspedtos)
@RequestMapping("/calculadora") //mapeamento raiz da calculadora
public class CalculadoraController {
    //classes de controlers representa o que ela vai representar

    //com o GetMapping vc deixa as funcões desse pacote calculadora disponibilizadas e sub mapeadas
//    @GetMapping("/olaMundo") //EndPonit para mapear chamar as classes e função via browser (http://localhost:8080/Teste)
//    public String olaMundo() {
//        return "ola Mundo";
//    }

//    @GetMapping("olaMundo2")
//    public String OlaMundo2() {
//        return "ola Mundo 2";
//    }

    //Observacao em controler so fica validação

    @Autowired //busca o objeto CalculadoraService criado
    private CalculadoraService calculadoraService;
    //com esse atributo com o @Autowired significa que estou fornecendo uma unica estancia do objeto CalculadoraServices
    //Single Patern

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora) {
        //System.out.println(calculadora.getNumeros()); //aqui da um posdt via Postman e depois olha no console

        //o ideal é criar uma dependencia da CAlculadora, garantindo que é o mesmo objeto, injecao de dependencia
        //CalculadoraService calculadoraService = new CalculadoraService();

        if (calculadora.getNumeros().size() <= 1) {
            //exececao exclusiva de controladores
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário no mínimo 2 números para Somar os números informados");
        }

        if (!validaEntradaNumeroNatural(calculadora)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É esperado apenas números Naturais (números inteiros positivos)");
        }

        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtratir(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário 2 números para efetuar a Subtração");
        }

        if (!validaEntradaNumeroNatural(calculadora)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É esperado apenas números Naturais (números inteiros positivos)");
        }

        return calculadoraService.subtrair(calculadora);

    }

    @PostMapping("/dividir")
    public RespostaDTO dividir(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário 2 números para efetuar a Divisão");
        }

        if (!validaEntradaNumeroNatural(calculadora)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É esperado apenas números Naturais (números inteiros positivos)");
        }

        return calculadoraService.dividir(calculadora);

    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário apenas 2 números para efetuar a Multiplicação");
        }

        if (!validaEntradaNumeroNatural(calculadora)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É esperado apenas números Naturais (números inteiros positivos)");
        }

        return calculadoraService.multiplicar(calculadora);
    }

    private boolean validaEntradaNumeroNatural(Calculadora calculadora) {
        boolean retorno = true;

        for (Integer numero : calculadora.getNumeros()) {
            if (numero < 0) {
                retorno = false;
            }
        }

        return retorno;
    }

    //@PostMapping("/") //Enviar para Http, usuaário envia post captura o post e devolve para o usuario
    //@PutMapping("/") //Atualização completa
    //@PatchMapping("/")//Atualiza parte das informações
    //@DeleteMapping("/") //Delete algo via Http
}
