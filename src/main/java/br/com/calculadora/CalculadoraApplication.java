package br.com.calculadora;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculadoraApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalculadoraApplication.class, args);
    }


    //classes para mapear dominio são chamads de controladoras
    //Controlers -> camada de entrada e saida do usuários
    //mapeamos os endereços de serviços



    //camada de servico -> regras e negócio

    //camada de persistence -> banco de dados



}
